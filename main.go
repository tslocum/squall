package main

import (
	"image/color"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
)

var center = pixel.V(384, 600)

var mapA = [][2]pixel.Vec{
	{pixel.V(50, 500), center},
	{pixel.V(50, 500), pixel.V(50, 400)},
	{pixel.V(50, 400), center},
	{pixel.V(50, 400), pixel.V(150, 400)},

	{pixel.V(150, 400), center},
	{pixel.V(150, 400), pixel.V(150, 300)},
	{pixel.V(150, 300), center},
	{pixel.V(150, 300), pixel.V(250, 300)},

	{pixel.V(250, 300), center},
	{pixel.V(250, 300), pixel.V(250, 200)},
	{pixel.V(250, 200), center},
	{pixel.V(250, 200), pixel.V(350, 200)},

	{pixel.V(350, 200), center},
	{pixel.V(350, 200), pixel.V(350, 100)},
	{pixel.V(350, 100), center},
	{pixel.V(350, 100), pixel.V(450, 100)},

	{pixel.V(450, 100), center},
	{pixel.V(450, 100), pixel.V(450, 200)},
	{pixel.V(450, 200), center},
	{pixel.V(450, 200), pixel.V(550, 200)},

	{pixel.V(550, 200), center},
	{pixel.V(550, 200), pixel.V(550, 300)},
	{pixel.V(550, 300), center},
	{pixel.V(550, 300), pixel.V(650, 300)},

	{pixel.V(650, 300), center},
	{pixel.V(650, 300), pixel.V(650, 400)},
	{pixel.V(650, 400), center},
	{pixel.V(650, 400), pixel.V(750, 400)},

	{pixel.V(750, 400), center},
	{pixel.V(750, 400), pixel.V(750, 500)},
	{pixel.V(750, 500), center},
}

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Platformer",
		Bounds: pixel.R(0, 0, 768, 768),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	win.Clear(color.Black)

	imd := imdraw.New(nil)

	for !win.Closed() {
		win.Clear(color.Black)

		imd.Clear()
		imd.EndShape = imdraw.NoEndShape

		for i := range mapA {
			drawPlayFieldBorder(imd, mapA[i][0], mapA[i][1])
		}

		for i := range mapA {
			drawPlayFieldLine(imd, mapA[i][0], mapA[i][1])
		}

		/*imd.Color = color.RGBA{R: 250, G: 10, B: 10, A: 255}
		// Draw any intersection points.
		for _, i := range r.IntersectionPoints(l) {
			imd.Push(i)
			imd.Circle(4, 0)
		}*/

		imd.Draw(win)
		win.Update()
	}
}

func drawPlayFieldBorder(imd *imdraw.IMDraw, v1 pixel.Vec, v2 pixel.Vec) {
	offset := float64(4)
	border := color.RGBA{R: 150, G: 50, B: 150, A: 255}
	borderThickness := float64(4)

	imd.Color = border

	imd.Push(v1.Add(pixel.V(offset, offset)))
	imd.Push(v2.Add(pixel.V(offset, offset)))
	imd.Line(borderThickness)

	imd.Push(v1.Add(pixel.V(0, offset)))
	imd.Push(v2.Add(pixel.V(0, offset)))
	imd.Line(borderThickness)

	imd.Push(v1.Add(pixel.V(offset, 0)))
	imd.Push(v2.Add(pixel.V(offset, 0)))
	imd.Line(borderThickness)

	imd.Push(v1.Sub(pixel.V(offset, offset)))
	imd.Push(v2.Sub(pixel.V(offset, offset)))
	imd.Line(borderThickness)

	imd.Push(v1.Sub(pixel.V(0, offset)))
	imd.Push(v2.Sub(pixel.V(0, offset)))
	imd.Line(borderThickness)

	imd.Push(v1.Sub(pixel.V(offset, 0)))
	imd.Push(v2.Sub(pixel.V(offset, 0)))
	imd.Line(borderThickness)
}

func drawPlayFieldLine(imd *imdraw.IMDraw, v1 pixel.Vec, v2 pixel.Vec) {
	imd.Color = color.RGBA{R: 75, G: 75, B: 225, A: 255}

	imd.Push(v1)
	imd.Push(v2)

	imd.Line(4)
}

func main() {
	pixelgl.Run(run)
}
